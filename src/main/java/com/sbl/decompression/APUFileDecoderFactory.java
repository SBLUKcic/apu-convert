package com.sbl.decompression;

import com.sbl.decompression.dependencies.*;
import com.sbl.decompression.dependencies.decoder.APUByteDecoder;
import com.sbl.decompression.dependencies.decoder.APUByteDecoderImpl;

public class APUFileDecoderFactory implements DecoderFactory {
  @Override
  public APUFileDecoder createAPUFileDecoder() {
    PointerExtractor extractor = new PointerExtractorImpl();
    APUChunkReader reader = new APUChunkReaderImpl();
    APUByteDecoder decoder = new APUByteDecoderImpl();
    AlignmentHistogramBuilder builder = new AlignmentHistogramBuilderImpl();
    return new APUFileDecoderImpl(decoder, reader, extractor, builder);
  }
}
