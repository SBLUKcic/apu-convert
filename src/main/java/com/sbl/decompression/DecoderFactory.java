package com.sbl.decompression;

/**
 *  Creates a valid APUFileDecoder with dependencies
 */
public interface DecoderFactory {

  /**
   * @return valid apuFileDecoder class
   */
  APUFileDecoder createAPUFileDecoder();
}
