package com.sbl.decompression.common;

public class DecodingResult {

    private char[] code;
    private int histogramLength;

    public DecodingResult(char[] code, int histogramLength) {
        this.code = code;
        this.histogramLength = histogramLength;
    }

    public char[] getCode() {
        return code;
    }

    public int getHistogramLength() {
        return histogramLength;
    }

}
