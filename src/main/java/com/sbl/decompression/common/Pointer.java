package com.sbl.decompression.common;

public class Pointer {

    private int stepSize;
    private int binSize;
    private String rname;
    private int bytePosition;

    public Pointer(int stepSize, int binSize, String rname, int bytePosition) {
        this.stepSize = stepSize;
        this.binSize = binSize;
        this.rname = rname;
        this.bytePosition = bytePosition;
    }

    public int getStepSize() {
        return stepSize;
    }

    public int getBinSize() {
        return binSize;
    }

    public String getRname() {
        return rname;
    }

    public int getBytePosition() {
        return bytePosition;
    }

}
