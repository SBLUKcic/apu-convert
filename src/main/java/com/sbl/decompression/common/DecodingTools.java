package com.sbl.decompression.common;



/** DecodingTools for working with SPEC part of the APUChunk
 */
 public class DecodingTools {

   //should not be instantiated.
  private DecodingTools() {
  }

  public static int convertFromByteArray2(byte byte1, byte byte2){
        return ((byte2 & 0xFF) << 8 | (byte1 & 0xFF));
    }

    public static int convertFromByteArray4(byte byte1, byte byte2, byte byte3, byte byte4){
        return byte1 << 24 | (byte2 & 0xFF) << 16 | (byte3 & 0xFF) << 8 | (byte4 & 0xFF);
    }

}
