package com.sbl.decompression.dependencies.decoder;


import com.sbl.compression.common.Token;
import com.sbl.decompression.common.DecodingResult;
import com.sbl.decompression.common.DecodingTools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class APUByteDecoderImpl implements APUByteDecoder {

    @Override
    public DecodingResult decodeBytes(byte[] bytes) {
        StringBuilder sb = new StringBuilder();

        Map<Character, Token> tokenLookup = new HashMap<>();
        for(Token token : Token.values()){
            tokenLookup.put(token.name().charAt(0), token);
        }

        int height = 0;
        int length = 0;
        int val;
        int histogramLength = 0;
        for (int i = 0; i < bytes.length; i++) {
            char token = (char) bytes[i];

            sb.append(token);
            boolean nonSpecial = false;
            Token t = tokenLookup.get(token);
            if(t != null){
                nonSpecial = true;
                height = t.getHeight();
                length = t.getLength();
            }

            if (nonSpecial) {
                //length
                if (length != 0 && length != 1) {
                    if (length == 8) {
                        //1 byte
                        sb.append(getPaddedString(String.valueOf(bytes[i + 1] & 0xFF), 3));
                        histogramLength += bytes[i + 1] & 0xFF;
                        i++;
                    } else if (length == 16) {
                        //2 bytes
                        val = DecodingTools.convertFromByteArray2(bytes[i + 1], bytes[i + 2]);
                        histogramLength += val;
                        sb.append(getPaddedString(String.valueOf(val), 5));
                        i += 2;
                    } else {
                        //4 bytes
                        val = DecodingTools.convertFromByteArray4(bytes[i + 1], bytes[i + 2], bytes[i + 3], bytes[i + 4]);
                        histogramLength += val;
                        sb.append(getPaddedString(String.valueOf(val), 10));

                        i += 4;
                    }
                } else {
                    histogramLength++;
                }

                //height
                if (height != 0 && height != 1) {
                    if (height == 8) {
                        //1 byte
                        sb.append(getPaddedString(String.valueOf(bytes[i + 1] & 0xFF), 3));
                        i++;
                    } else if (height == 16) {
                        //2 bytes
                        val = DecodingTools.convertFromByteArray2(bytes[i + 1], bytes[i + 2]);
                        sb.append(getPaddedString(String.valueOf(val), 5));
                        i += 2;
                    } else {
                        //4 bytes
                        val = DecodingTools.convertFromByteArray4(bytes[i + 1], bytes[i + 2], bytes[i + 3], bytes[i + 4]);
                        sb.append(getPaddedString(String.valueOf(val), 10));
                        i += 4;
                    }
                }
            } else {
                switch (token) {
                    case 'R': {
                        int numReads = (int) bytes[i + 1] & 0xFF;
                        i++;
                        sb.append(getPaddedString(String.valueOf(numReads), 3));
                        for (int j = 0; j < numReads; j++) {
                            int nextNum = bytes[i + 1] & 0xFF;
                            sb.append(getPaddedString(String.valueOf(nextNum), 3));
                            histogramLength++;
                            i++;
                        }
                        break;
                    }
                    case 'S': {
                        int numReads = (int) bytes[i + 1] & 0xFF;
                        i++;
                        sb.append(getPaddedString(String.valueOf(numReads), 3));
                        for (int j = 0; j < numReads; j++) {
                            histogramLength++;

                            sb.append(getPaddedString(String.valueOf(DecodingTools.convertFromByteArray2(bytes[i + 1], bytes[i + 2])), 5));
                            i += 2;

                        }
                        break;
                    }
                    case 'T': {
                        sb.append(getPaddedString(String.valueOf(DecodingTools.convertFromByteArray4(bytes[i + 1], bytes[i + 2], bytes[i + 3], bytes[i + 4])), 10));
                        i += 4;
                        sb.append(getPaddedString(String.valueOf(DecodingTools.convertFromByteArray4(bytes[i + 1], bytes[i + 2], bytes[i + 3], bytes[i + 4])), 10));
                        i += 4;
                        break;
                    }
                    case 'U': {
                        sb.append(getPaddedString(String.valueOf(DecodingTools.convertFromByteArray4(bytes[i + 1], bytes[i + 2], bytes[i + 3], bytes[i + 4])), 10));
                        i += 4;
                        break;
                    }
                    case 'V': {
                        List<Byte> VBytes = new ArrayList<>();
                        boolean escapeFound = false;
                        while (!escapeFound) {
                            if (i + 1 < bytes.length) {
                                if (bytes[i + 1] == 0) escapeFound = true;
                                else {
                                    VBytes.add(bytes[i + 1]);
                                    i += 1;
                                }
                            }
                        }
                        for (byte b : VBytes) {
                            sb.append((char) b);
                        }
                        sb.append(getPaddedString(String.valueOf(bytes[i + 1] & 0xFF), 3));
                        i += 1;
                        break;
                    }
                    case 'W': {
                        for (int j = 0; j < 6; j++) {
                            sb.append("000");
                            i += 1;
                        }
                        sb.append(getPaddedString(String.valueOf(DecodingTools.convertFromByteArray4(bytes[i + 1], bytes[i + 2], bytes[i + 3], bytes[i + 4])), 10));
                        i += 4;
                        break;
                    }
                    case 'X': {
                        sb.append(getPaddedString(String.valueOf(bytes[i + 1] & 0xFF), 3));
                        i += 1;
                        //get length of the statement
                        int statementLength = bytes[i + 1] & 0xFF;
                        sb.append(getPaddedString(String.valueOf(statementLength), 3));
                        i += 1;
                        for (int j = i + 1; j < i + 1 + statementLength; j++) {
                            sb.append((char) bytes[j]);
                        }
                        i += statementLength;
                        sb.append(getPaddedString(String.valueOf(DecodingTools.convertFromByteArray2(bytes[i + 1], bytes[i + 2])), 5));
                        i += 2;
                        //endseq
                        int endLength = bytes[i + 1];
                        sb.append(getPaddedString(String.valueOf(endLength), 3));
                        i += 1;
                        if (endLength != 0) {
                            for (int j = i + 1; j < i + 1 + endLength; j++) {
                                sb.append((char) bytes[j]);
                            }
                            i += endLength;
                        }

                        //flankseq
                        int flankLength = bytes[i + 1];
                        sb.append(getPaddedString(String.valueOf(flankLength), 3));
                        i += 1;
                        if (flankLength != 0) {
                            for (int j = i + 1; j < i + 1 + flankLength; j++) {
                                sb.append((char) bytes[j]);
                            }
                            i += flankLength;
                        }
                        break;
                    }
                    case 'Y': {
                        //must be Y
                        sb.append(getPaddedString(String.valueOf(DecodingTools.convertFromByteArray4(bytes[i + 1], bytes[i + 2], bytes[i + 3], bytes[i + 4])), 10));
                        i += 4;
                        break;
                    }
                }
            }

        }

        return new DecodingResult(sb.toString().toCharArray(), histogramLength);
    }

    private static String getPaddedString(String s, int max){
        StringBuilder b = new StringBuilder(max);
        for(int i = max - s.length(); i > 0; i--){
            b.append('0');
        }
        b.append(s);
        return b.toString();
    }

}
