package com.sbl.decompression.dependencies.decoder;

import com.sbl.decompression.common.DecodingResult;

public interface APUByteDecoder {

    DecodingResult decodeBytes(byte[] bytes);
}
