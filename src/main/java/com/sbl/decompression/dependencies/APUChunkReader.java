package com.sbl.decompression.dependencies;

import java.io.File;
import java.io.IOException;

public interface APUChunkReader {

    byte[] readChunk(File apuFile, int bytePosition) throws IOException;


    /** Looks to retrieve just part of the histogram, using the W elements as guides
     * @param apuFile file to use
     * @param bytePosition start of the relevant histogram
     * @param targetPosition position to look for within the file
     * @return byte array to be decoded.
     * @throws IOException
     */
    byte[] readChunk(File apuFile, int bytePosition, int targetPosition) throws IOException;
}
