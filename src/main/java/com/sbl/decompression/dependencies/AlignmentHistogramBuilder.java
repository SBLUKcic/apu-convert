package com.sbl.decompression.dependencies;

import com.sbl.common.AlignmentHistogram;
import com.sbl.decompression.common.DecodingResult;

public interface AlignmentHistogramBuilder {

    AlignmentHistogram buildAlignmentHistogram(DecodingResult result) throws Exception;

}
