package com.sbl.decompression.dependencies;



import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class APUChunkReaderImpl implements APUChunkReader {


    /** reads the relavent chunk from the input file, returns a byte array
     *  Jumps to position, given by byte array
     *  Reads 4 bytes for the TYPE
     *  Reads 4 bytes for the SIZE
     *  Converts the SIZE into an integer size, (ByteBuffer wrapper)
     *  Reads the Size integer value bytes
     * @param apuFile file to read
     * @param bytePosition position to jump to.
     * @return byte array representation of the file
     */
    @Override
    public byte[] readChunk(File apuFile, int bytePosition) throws IOException {
        //read past header and pointer
        byte[] header = new byte[8];
        byte[] pntrtype = new byte[4];
        byte[] pntrsize = new byte[4];

        try{
            FileInputStream fis = new FileInputStream(apuFile);

            fis.read(header);
            fis.read(pntrtype);
            fis.read(pntrsize);
            int pntrBytes = convertFromByteArray4(pntrsize);

            byte[] skip = new byte[pntrBytes];
            fis.read(skip);

            byte[] skip2 = new byte[bytePosition];
            fis.read(skip2);

            //now at the chunk
            byte[] chunkTYPE = new byte[4];
            fis.read(chunkTYPE);
            byte[] chunkSIZE = new byte[4];
            fis.read(chunkSIZE);

            int chunksize = convertFromByteArray4(chunkSIZE);

            byte[] chunk = new byte[chunksize];
            fis.read(chunk);

            fis.close();
            return chunk;

        }catch (IOException ioe){
            throw new IOException("Cannot read APU File");
        }

    }

    @Override
    public byte[] readChunk(File apuFile, int bytePosition, int targetPosition) throws IOException {
        //read past header and pointer
        byte[] header = new byte[8];
        byte[] pntrtype = new byte[4];
        byte[] pntrsize = new byte[4];

        try{
            FileInputStream fis = new FileInputStream(apuFile);

            fis.read(header);
            fis.read(pntrtype);
            fis.read(pntrsize);
            int pntrBytes = convertFromByteArray4(pntrsize);

            byte[] skip = new byte[pntrBytes];
            fis.read(skip);

            byte[] skip2 = new byte[bytePosition];
            fis.read(skip2);

            //now at the chunk
            byte[] chunkTYPE = new byte[4];
            fis.read(chunkTYPE);
            byte[] chunkSIZE = new byte[4];
            fis.read(chunkSIZE);

            int chunksize = convertFromByteArray4(chunkSIZE);

            byte[] chunk = new byte[chunksize];
            fis.read(chunk);

            int lastMarkerPosition = 0;

            List<Byte> chunkInfo = new ArrayList<>();
            boolean wFound = false;

            for(int i = 0; i < chunk.length; i++){
                if(chunk[i] == 'W'){
                    wFound = true;
                    //hit a marker
                    byte[] arr = Arrays.copyOfRange(chunk, i+7, i+11);
                    int marker = convertFromByteArray4(arr);

                    if(targetPosition < marker){
                        //found the one after
                        byte[] info = new byte[chunkInfo.size()];
                        for(int j = 0; j < chunkInfo.size(); j++){
                            info[j] = chunkInfo.get(j);
                        }

                        //find the next marker
                        for(int j = i+1; j < chunk.length; j++){
                            if(chunk[j] == 'W') {
                                return combine(info, Arrays.copyOfRange(chunk, lastMarkerPosition, j+11));
                            }
                        }

                        return combine(info, Arrays.copyOfRange(chunk, lastMarkerPosition, chunk.length));
                    }

                    lastMarkerPosition = i;
                    i+=10;

                }

                if(!wFound){
                    chunkInfo.add(chunk[i]);
                }
            }

            fis.close();
            return new byte[0];

        }catch (IOException ioe){
            throw new IOException("Cannot read APU File");
        }
    }

    /** Convert 4 bytes into integer
     * @param bytes bytes to convert
     * @return 4 bit signed integer
     */
    private int convertFromByteArray4(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getInt();
    }

    private static byte[] combine(byte[] a, byte[] b){
        int length = a.length + b.length;
        byte[] result = new byte[length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

}
