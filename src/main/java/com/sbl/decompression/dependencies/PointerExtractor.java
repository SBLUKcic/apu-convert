package com.sbl.decompression.dependencies;

import com.sbl.decompression.common.Pointer;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface PointerExtractor {

    List<Pointer> extractPointers(File apuFile) throws IOException;

}
