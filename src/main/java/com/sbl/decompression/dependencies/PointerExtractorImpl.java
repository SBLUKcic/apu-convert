package com.sbl.decompression.dependencies;

import com.sbl.decompression.common.Pointer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class PointerExtractorImpl implements PointerExtractor {

    @Override
    public List<Pointer> extractPointers(File apuFile) throws IOException {
        List<Pointer> chunks = new ArrayList<>();
        FileInputStream fis = new FileInputStream(apuFile);
        //skip past the first 8 bytes
        byte[] header = new byte[8];
        fis.read(header);

        byte[] TYPE = new byte[4];
        fis.read(TYPE);
        if (!checkPointerTypeIsValid(TYPE)) throw new IOException("Pointer chunk invalid");

        byte[] SIZE = new byte[4];
        fis.read(SIZE);
        int noBytes = convertFromByteArray4(SIZE);

        byte[] allChunks = new byte[noBytes];
        fis.read(allChunks);

        for (int i = 0; i < allChunks.length; i++) {
            i += 3; //skip the res
            //parse the step size;
            StringBuilder stepSizeBuilder = new StringBuilder();
            for (int j = 0; j < 10; j++) {
                stepSizeBuilder.append((char) allChunks[i + j]);
            }
            i += 10;

            int stepSize = Integer.parseInt(stepSizeBuilder.toString().replaceFirst("^0+(?!$)", ""));

            //parse the bin size

            StringBuilder binSizeBuilder = new StringBuilder();
            for (int j = 0; j < 10; j++) {
                binSizeBuilder.append((char) allChunks[i + j]);
            }
            i += 10;

            int binSize = Integer.parseInt(binSizeBuilder.toString());

            //skip the Z
            i += 1;

            //get the rname length
            StringBuilder rnameLengthBuilder = new StringBuilder();
            for (int j = 0; j < 3; j++) {
                rnameLengthBuilder.append((char) allChunks[i + j]);
            }
            i += 3;

            int len = Integer.parseInt(rnameLengthBuilder.toString());

            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < len; j++) {
                sb.append((char) allChunks[i + j]);
            }
            i += len;

            //parse the byte position

            StringBuilder byteSizeBuilder = new StringBuilder();
            for (int j = 0; j < 10; j++) {
                byteSizeBuilder.append((char) allChunks[i + j]);
            }
            i += 9;

            int bytePosition = Integer.parseInt(byteSizeBuilder.toString().replaceFirst("^0+(?!$)", ""));

            if(stepSize == 1 && binSize == 1) { //only accept the 1,1 resolution bin sizes
                Pointer chunk = new Pointer(stepSize, binSize, sb.toString(), bytePosition);
                chunks.add(chunk);
            }
        }
        return chunks;
    }

    /** Checks the Pointers TYPE is valid. checks against ASCII characters 'P','N','T','R'.
     * @param TYPE bytes to check
     * @return true or false if match
     */
    private static boolean checkPointerTypeIsValid(byte[] TYPE) {
        return TYPE[0] == 80 && TYPE[1] == 78 && TYPE[2] == 84 && TYPE[3] == 82;
    }

    /** Converts from 4 bytes into a 32 bit unsigned integer
     * @param bytes byte array to convert
     * @return integer representation of bytes
     */
    private static int convertFromByteArray4(byte[] bytes){
        return ByteBuffer.wrap(bytes).getInt();
    }
}

