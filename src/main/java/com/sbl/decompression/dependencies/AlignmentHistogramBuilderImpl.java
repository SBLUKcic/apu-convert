package com.sbl.decompression.dependencies;

import com.sbl.common.AlignmentHistogram;
import com.sbl.common.Resolution;
import com.sbl.compression.common.AlignmentInfo;
import com.sbl.compression.common.Token;
import com.sbl.decompression.common.DecodingResult;

public class AlignmentHistogramBuilderImpl implements AlignmentHistogramBuilder {

    @Override
    public AlignmentHistogram buildAlignmentHistogram(DecodingResult result) throws Exception {

    char[] values = result.getCode();
    int len = result.getHistogramLength();
    //Map<Integer, Integer> ACH = new TreeMap<>();
    int[] ach = new int[len];
    String footprintDescription = "";
    //get the first position on the rname.
    StringBuilder rname = new StringBuilder();

    int stepSize = 1;
    int binSize = 1;


    int startPos = 0;
    int code = 0;
    int currentPos = 0;
    boolean startFound = false;
    int endPos = 0;
    int i;
        try {

            //todo - fix the APU conversion

        for (i = 0; i < values.length; i++) {

            if(values[i] == 'T'){
                stepSize = charArrayToInt(values, i+1, i+11);
                binSize = charArrayToInt(values, i+11, i+21);
                i+=21;
            }

            //loop through until W -> get the position
            if (values[i] == 'W' && !startFound) {

                //skip the 18 0's
                startPos = charArrayToInt(values, i+18, i+29);
                // currentPos = startPos;
                i += 28;
                startFound = true;
            }

            if(values[i] == 'Y' && startFound){
                endPos = charArrayToInt(values, i+1, i+11);
                i+=11;
            }

            if(values[i] == 'X'){
                StringBuilder sb = new StringBuilder();

                sb.append('X');
                code = charArrayToInt(values, i+1, i+4);
                sb.append(String.format("%03d", code));


                //do something with the code??
                int num = charArrayToInt(values, i+4, i+7);
                i += 7; //skip past the X, code, and num
                sb.append(String.format("%03d", num));

                sb.append(new String(values).substring(i, i+num));
                i+=num;
                //read in the next 5 bases
                num = charArrayToInt(values, i, i+5);
                i+=5;
                sb.append(String.format("%05d", num));
                sb.append(num != 0 ? new String(values).substring(i+1,i+num) : "");
                sb.append("000000");
                footprintDescription = sb.toString();
                i += num - 1; //-1, as i++ at the end of loop
            }

            if(values[i] == 'V'){

                //skip past the RNAME, do with it what you will
                for(int j = i+1; j < values.length; j++){
                    rname.append(values[j]);
                    i++;
                    if(values[j+1] == '0' && values[j+2] == '0' && values[j+3] == '0' && values[j+4] != '0'){
                        break;
                    }
                }

                i+=2;
            }

            //loop past Y
            if (startFound && ((values[i] >= 'A' && values[i] <= 'S') || values[i] == 'Z')) {
                char character = values[i];
                if (character != 'S' && character != 'R') {

                    int length = 1;
                    int frequency = 0;
                    int lengthBits = 0;
                    int heightBits = 0;
                    for (Token token : Token.values()) {
                        if (token.name().equals(String.valueOf(character))) {
                            lengthBits = token.getLength();
                            heightBits = token.getHeight();
                        }
                    }

                    switch (lengthBits) {
                        case 1:
                            length = 1;
                            break;
                        case 8:
                            length = charArrayToInt(values, i+1, i+4);
                            i += 3;
                            break;
                        case 16:
                            length = charArrayToInt(values, i+1, i+6);
                            i += 5;
                            break;
                        case 32:
                            length = charArrayToInt(values, i+1, i+11);
                            i += 10;
                    }

                    switch (heightBits) {
                        case 1:
                            frequency = 1;
                            break;
                        case 8:
                            frequency = charArrayToInt(values, i+1, i+4);
                            i += 3;
                            break;
                        case 16:
                            frequency = charArrayToInt(values, i+1, i+6);
                            i += 5;
                            break;
                        case 32:
                            frequency = charArrayToInt(values, i+1, i+11);
                            i += 10;
                    }

                    for (int j = 0; j < length; j++) {
                        ach[currentPos] = frequency;
                        currentPos++;
                    }

                } else {
                    if (character == 'R') {
                        int numBins = charArrayToInt(values, i+1, i+4);
                        i += 3;
                        for (int j = 0; j < numBins; j++) {
                            ach[currentPos] = charArrayToInt(values, i+1, i+4);
                            currentPos++;
                            i += 3;
                        }

                    } else {
                        int numBins = charArrayToInt(values, i+1, i+4);
                        i += 3;
                        for (int j = 0; j < numBins; j++) {
                            ach[currentPos] = charArrayToInt(values, i+1, i+6);
                            currentPos++;
                            i += 5;
                        }

                    }
                }
            }
            //Read in the tokens, with values. pass to function to add to ACH.
        }

    } catch (Exception nfe) {
       throw new Exception("ACH Building Error: " + nfe.getMessage());
    }

    if(ach.length == 0) throw new Exception("No histogram to return");

    return new AlignmentHistogram(ach , startPos, endPos, rname.toString(), new Resolution(stepSize, binSize), new AlignmentInfo(code, footprintDescription));
}



    /** Converts an input chart array into a valid integer. 'i.e' 000000123, -> 123
     *  Must supply full character array and start and end of the section to convert.
     *  Avoids creating extra objects
     * @param data full character sequence of the input string, tokens and all.
     * @param start start of the sequence of convert
     * @param end end of the sequence to convert
     * @return valid int, or 0.
     * @throws NumberFormatException invalid digit, cannot be a number
     */
    private static int charArrayToInt(char[] data, int start, int end) throws NumberFormatException{
        int result = 0;
        for(int i = start; i < end; i++){
            int digit = (int)data[i] - (int)'0';
            if((digit < 0) || (digit > 9)) throw new NumberFormatException("cannot convert digit: " + digit);
            result *= 10;
            result += digit;
        }
        return result;
    }
}
