package com.sbl.decompression;

import com.sbl.common.AlignmentHistogram;
import com.sbl.decompression.dependencies.AlignmentHistogramBuilder;
import com.sbl.decompression.common.DecodingResult;
import com.sbl.decompression.common.Pointer;
import com.sbl.decompression.dependencies.PointerExtractor;
import com.sbl.decompression.dependencies.decoder.APUByteDecoder;
import com.sbl.decompression.dependencies.APUChunkReader;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * {@inheritDoc}
 */
public class APUFileDecoderImpl implements APUFileDecoder {

  private APUByteDecoder apuByteDecoder;
  private APUChunkReader apuChunkReader;
  private PointerExtractor pointerExtractor;
  private AlignmentHistogramBuilder alignmentHistogramBuilder;

  public APUFileDecoderImpl(APUByteDecoder apuByteDecoder,
                            APUChunkReader apuChunkReader,
                            PointerExtractor pointerExtractor,
                            AlignmentHistogramBuilder builder) {
    this.apuByteDecoder = apuByteDecoder;
    this.apuChunkReader = apuChunkReader;
    this.pointerExtractor = pointerExtractor;
    this.alignmentHistogramBuilder = builder;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Pointer> getFileInfo(File apuFile) throws IOException {
    return pointerExtractor.extractPointers(apuFile);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AlignmentHistogram retrieveAPUChunk(File apuFile, Pointer pointer) throws Exception {
    byte[] bytes = apuChunkReader.readChunk(apuFile, pointer.getBytePosition());
    DecodingResult result = apuByteDecoder.decodeBytes(bytes);
    return alignmentHistogramBuilder.buildAlignmentHistogram(result);
  }

  /**
   * {@inheritDoc}
   * @param apuFile file containing apu info
   * @param pointer pointer information
   * @param start start position of the information to retrieve
   * @param window no. of data positions to retrieve after the specified start
   * @return
   * @throws IOException
   */
  @Override
  public AlignmentHistogram retrievePositions(File apuFile, Pointer pointer, int start, int window) throws Exception {
    byte[] bytes = apuChunkReader.readChunk(apuFile, pointer.getBytePosition(), start);
    DecodingResult result = apuByteDecoder.decodeBytes(bytes);
    AlignmentHistogram histogram = alignmentHistogramBuilder.buildAlignmentHistogram(result);

    int[] cutHist = new int[window];
    int j = 0;

    for(int i = 0; i < histogram.getHistogram().length; i++){
      if(i + histogram.getFirstPosition() >= start){
        if(j == window){
          break;
        }
        cutHist[j] = histogram.getHistogram()[i];
        j++;
      }
    }

    return new AlignmentHistogram(cutHist, start, start + window, histogram.getRname(), histogram.getResolution(), histogram.getAlignmentInfo());
  }
}
