package com.sbl.decompression;

import com.sbl.common.AlignmentHistogram;
import com.sbl.decompression.common.DecodingResult;
import com.sbl.decompression.common.Pointer;
import com.sbl.exceptions.NoSuchChunkException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/** Interface to work with a valid APU file
 *
 */
public interface APUFileDecoder {

  /** Gets the information on which alignment histograms are in the file
   * @param apuFile file containing the apu information
   * @return List collection of Pointer objects, containing information about each alignment histogram
   * @throws IOException Some issue readers the input file.
   */
  List<Pointer> getFileInfo(File apuFile) throws IOException;

  /** Retrieve the alignment histogram information from the file, according to the pointer
   * @param apuFile file containing the apu information
   * @param pointer pointer information (should be taken via the getFileInfo method)
   * @return the relevant APUChunk, containing the histogram
   * @throws NoSuchChunkException Cannot find the chunk in the file
   */
  AlignmentHistogram retrieveAPUChunk(File apuFile, Pointer pointer) throws Exception;


  /** Retrieve just part of the histogram information - retrieved via the W elements.
   * @param apuFile file containing apu info
   * @param pointer pointer information
   * @param start start position of the information to retrieve
   * @param window window size of the data to retrieve
   * @return Decoding result with the relevant information
   * @throws IOException something went wrong
   */
  AlignmentHistogram retrievePositions(File apuFile, Pointer pointer, int start, int window) throws Exception;


}
