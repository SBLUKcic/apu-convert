package com.sbl.compression;

/**
 *  implementation should handle creation of array converter objects - including all dependencies
 */
public interface ConverterFactory {


  /** creates a new array converter
   * @return array converter with valid dependencies.
   */
  ArrayConverter createArrayConverter();


}
