package com.sbl.compression.dependencies;

import com.sbl.common.APUChunk;
import com.sbl.compression.common.Token;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class APUFormatEncoderImpl implements APUFormatEncoder {

  private byte[] byteList;
  private int pointer;

  @Override
  public APUChunk convertToAPU(char[] rawSPEC) {
    int i = 0;
    //work out the size in advance.
    StringBuilder rname = new StringBuilder();

    byteList = new byte[900000000]; //longest chromosone = mm01, ~ 170,000,000 in mice, 250,000,000 in humans
    pointer = 0;
    try {
      //loop through the raw string
      int length = 0;
      int height = 0;
      for (i = 0; i < rawSPEC.length; i++) {
        //read the token from the string
        char token = rawSPEC[i];

        byteList[pointer] = ((byte) (token & 0xFF));
        pointer++;

        //find the length and height in bytes of the valid token
        boolean nonSpecial = false;
        for (Token t : Token.values()) {
          if (token == t.name().charAt(0)) {
            nonSpecial = true;
            height = t.getHeight();
            length = t.getLength();
          }
        }

        if (nonSpecial) {
          //read the height and length values
          if (length != 0 && length != 1) {
            if (length == 8) {
              //1 byte. just convert to byte and add
              byteList[pointer] = ((byte) (charArrayToInt(rawSPEC, i + 1, i + 4)));
              pointer++;
              i += 3;
            } else if (length == 16) {
              //2 bytes. convert to byte array and add each sequencially
              encode2BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 6));
              i += 5;
            } else {
              //4 bytes. convert to byte array and add each sequencially
              encode4BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 11));
              i += 10;
            }

          }

          //encode any heights
          if (height != 0 && height != 1) {
            if (height == 8) {
              byteList[pointer] = ((byte) (charArrayToInt(rawSPEC, i + 1, i + 4)));
              pointer++;
              i += 3;
            } else if (height == 16) {
              encode2BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 6));
              i += 5;
            } else {
              encode4BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 11));
              i += 10;
            }

          }

        } else if (token == 'R') {
          int numReads = charArrayToInt(rawSPEC, i + 1, i + 4);
          byteList[pointer] = ((byte) (numReads));
          pointer++;
          i += 3;
          for (int j = 0; j < numReads; j++) {
            byteList[pointer] = (byte) (charArrayToInt(rawSPEC, i + 1, i + 4));
            pointer++;
            i += 3;
          }
        } else if (token == 'S') {
          int numReads = charArrayToInt(rawSPEC, i + 1, i + 4);
          byteList[pointer] = ((byte) (numReads));
          pointer++;
          i += 3;
          for (int j = 0; j < numReads; j++) {
            encode2BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 6));
            i += 5;
          }
        } else if (token == 'T') {
          encode4BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 11));
          i += 10;
          encode4BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 11));
          i += 10;
        } else if (token == 'U') {
          encode4BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 11));
          i += 10;
        } else if (token == 'V') {
          for (int j = i; j < rawSPEC.length; j++) {
            if (rawSPEC[j] == '0' && rawSPEC[j + 1] == '0' && rawSPEC[j + 2] == '0' && rawSPEC[j + 3] != '0') { //check for trailing zeros

              for (int z = i + 1; z < j; z++) {
                rname.append(rawSPEC[z]);
                byteList[pointer] = (byte) (rawSPEC[z] & 0xFF);
                pointer++;
                i++;
              }
              byteList[pointer] = ((byte) 0);
              pointer++;
              i += 3;
              break;
            }
          }
        } else if (token == 'W') {
          //encode the 6 escape characters
          for (int j = 0; j < 6; j++) {
            byteList[pointer] = (byte) (charArrayToInt(rawSPEC, i + 1, i + 4));
            pointer++;
            i += 3;
          }

          //encode the 4 Bit number
          encode4BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 11));
          i += 10;

        } else if (token == 'X') {
          //code
          byteList[pointer] = (byte) (charArrayToInt(rawSPEC, i + 1, i + 4));
          pointer++;
          i += 3;

          //statement
          int leng = charArrayToInt(rawSPEC, i + 1, i + 4);
          i += 3;
          byteList[pointer] = (byte) (leng);
          pointer++;
          for (int j = i + 1; j < i + 1 + leng; j++) {
            byteList[pointer] = ((byte) rawSPEC[j]);
            pointer++;
          }
          i += leng;

          //extension
          encode2BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 6));

          i += 5;
          //endseq
          int endLength = charArrayToInt(rawSPEC, i + 1, i + 4);
          byteList[pointer] = ((byte) endLength);
          pointer++;
          i += 3;
          if (endLength != 0) {
            for (int j = i + 1; j < i + 1 + endLength; j++) {
              byteList[pointer] = ((byte) rawSPEC[j]);
              pointer++;
            }
            i += endLength;
          }

          //endseq
          int flankLength = charArrayToInt(rawSPEC, i + 1, i + 4);
          byteList[pointer] = ((byte) flankLength);
          pointer++;
          i += 3;
          if (flankLength != 0) {
            for (int j = i + 1; j < i + 1 + flankLength; j++) {
              byteList[pointer] = ((byte) rawSPEC[j]);
              pointer++;
            }
            i += flankLength;
          }


        } else if (token == 'Y') {
          encode4BitStringDecimal(charArrayToInt(rawSPEC, i + 1, i + 11));
          i += 10;
        }
      }
    } catch (NumberFormatException nfe) {
      for (int j = i - 50; j < i + 50; j++) {
        if (j == i) System.out.println("\n");
        System.out.print(rawSPEC[j] + ",");
        if (j == i) System.out.println("\n");
      }
      System.out.println("\n");
      System.out.println(nfe.getMessage());
    }

    //convert to array

    //encode
    byte[] encodedSPEC = Arrays.copyOfRange(byteList, 0, pointer);
    int SIZE = encodedSPEC.length;
    byte[] type = {(byte) 84, (byte) 89, (byte) 80, (byte) 60};
    int TYPE = ByteBuffer.wrap(type).getInt();

    return new APUChunk(TYPE, SIZE, encodedSPEC, rname.toString());
  }

  private static int charArrayToInt(char[] data, int start, int end) throws NumberFormatException {
    int result = 0;
    for (int i = start; i < end; i++) {
      int digit = (int) data[i] - (int) '0';
      if ((digit < 0) || (digit > 9)) throw new NumberFormatException("Cannot convert digit: " + digit);
      result *= 10;
      result += digit;
    }
    return result;
  }

  private void encode2BitStringDecimal(int i) {
    byteList[pointer] = (byte) (i & 0xFF);
    byteList[pointer + 1] = (byte) ((i >> 8) & 0xFF);
    pointer += 2;
  }

  private void encode4BitStringDecimal(int i) {
    byteList[pointer] = (byte) (i >> 24 & 0xFF);
    byteList[pointer + 1] = (byte) (i >> 16 & 0xFF);
    byteList[pointer + 2] = (byte) (i >> 8 & 0xFF);
    byteList[pointer + 3] = (byte) (i & 0xFF);
    pointer += 4;
  }
}
