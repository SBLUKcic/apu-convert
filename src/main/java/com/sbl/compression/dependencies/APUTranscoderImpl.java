package com.sbl.compression.dependencies;

import com.sbl.common.APUChunk;
import com.sbl.common.AlignmentHistogram;
import com.sbl.common.Resolution;
import com.sbl.compression.common.AlignmentInfo;
import com.sbl.compression.common.EncodingData;
import com.sbl.compression.common.TranscodingToken;
import com.sbl.exceptions.InvalidTokenException;

import java.nio.ByteBuffer;
import java.util.*;

public class APUTranscoderImpl implements APUTranscoder {

  private static final NavigableSet<Integer> upperBoundaries;

  static {
    upperBoundaries = new TreeSet<>();
    upperBoundaries.add((1 << 8) - 1); //255
    upperBoundaries.add((1 << 16) - 1); //65535
  }

  private static int log2ceil(int value) {
    if (value <= 0) {
      throw new IllegalArgumentException("value must be positive");
    } else {
      return 32 - Integer.numberOfLeadingZeros(value - 1);
    }
  }


  @Override
  public String transcode(AlignmentHistogram alignmentHistogram, List<EncodingData> encodingData) {
    StringBuilder specBuilder = new StringBuilder();

    Resolution resolution = alignmentHistogram.getResolution();
    String rname = alignmentHistogram.getRname();

    //loop through -> bin > 1 or a single bin of 1 -> use regular characters
    //runs of all 1's -> if all the H's are less than 255, use R
    //runs of all 1's -> if all the H's are less than 65535, use S.

    int posTracker = alignmentHistogram.getFirstPosition();
    int byteCount = 0;

    List<EncodingData> run = new ArrayList<>();
    for (EncodingData data : encodingData) {
      byteCount += data.getLength();

      if (byteCount >= 1000) {
        //append a new W element and reset the byteCount;
        posTracker += byteCount;
        byteCount = 0;
        specBuilder.append(String.format("W000000000000000000%010d", posTracker));
      }

      if (data.getLength() == 1) {
        //can continue the run.
        run.add(data);
      } else {
        //must stop the run
        if (run.size() == 1) { //encode the single run = 1 element
          specBuilder.append(transcodeUsingRegularElements(run.get(0)));
          run.clear();
        } else if (run.size() > 0) {
          specBuilder.append(transcodeSpecialOrMultipleRegularElements(run));
          run.clear();
        }

        //transcode the single greater than 1 bin run.
        specBuilder.append(transcodeUsingRegularElements(data));
      }
    }

    //encode last if run is not empty
    if (run.size() > 1) {
      //must encode the current run list and then encode the single greater than 1 list
      //count the heights, if less than 255, special R, if less than 65535 but more than 255 use S.
      //if greater than 65535 -> split the list until it fits and encode in multiple Special characters
      specBuilder.append(transcodeSpecialOrMultipleRegularElements(run));
      run.clear();
    } else if (run.size() == 1) { //encode the single run = 1 element
      specBuilder.append(transcodeUsingRegularElements(run.get(0)));
      run.clear();
    }

    //append the escape sequence
    specBuilder.append("000000000000000000");

    StringBuilder fullSPEC = new StringBuilder();

    //append T - resolution
    fullSPEC.append(String.format("T%010d%010d", resolution.getStepSize(), resolution.getBinSize()));
    //append V - name
    fullSPEC.append(String.format("V%s000", rname));
    //append X - footprint
    fullSPEC.append("X");

    //footprint code
    fullSPEC.append(String.format("%03d", alignmentHistogram.getAlignmentInfo().getCode()));

    //statement
    fullSPEC.append(String.format("%03d%s", alignmentHistogram.getAlignmentInfo().getStatementString().length(), alignmentHistogram.getAlignmentInfo().getStatementString()));

    //extension
    fullSPEC.append(String.format("%05d", alignmentHistogram.getAlignmentInfo().getExtensionValue()));

    //endseq
    if (alignmentHistogram.getAlignmentInfo().getEndseqQuery() != null) {
      fullSPEC.append(String.format("%03d%s", alignmentHistogram.getAlignmentInfo().getEndseqQuery().length(), alignmentHistogram.getAlignmentInfo().getEndseqQuery()));

      //flankseq

    }else if(alignmentHistogram.getAlignmentInfo().getFlankseqQuery() != null){
      fullSPEC.append(String.format("%03d%s", alignmentHistogram.getAlignmentInfo().getFlankseqQuery().length(), alignmentHistogram.getAlignmentInfo().getEndseqQuery()));
    }else {
      fullSPEC.append("000000");
    }
    //append W - start Pos
    fullSPEC.append(String.format("W000000000000000000%010d", alignmentHistogram.getFirstPosition()));

    //append Y
    fullSPEC.append(String.format("Y%010d", alignmentHistogram.getLastPosition()));

    //append the body
    fullSPEC.append(specBuilder.toString());

    return fullSPEC.toString();


  }

  /**
   * Takes in a run of multiple encoding data points and finds the most efficient way of writing it
   * All encoding data elements in this run will have length 1.
   *
   * @param run list of encoding data points
   * @return String to append to the chunk
   */
  private static String transcodeSpecialOrMultipleRegularElements(List<EncodingData> run) {

    StringBuilder specBuilder = new StringBuilder();

    Integer maxiumumUpperBoundary = Integer.MIN_VALUE; //can be null.
    for (EncodingData data : run) {
      if (data.getFrequency() > maxiumumUpperBoundary) {
        maxiumumUpperBoundary = upperBoundaries.ceiling(data.getFrequency()); //if above 65535, will return null.
        if (maxiumumUpperBoundary == null) {
          break; //breaks out if above 65535
        }
      }
    }

    OptionalInt specialSize;
    if (maxiumumUpperBoundary == null) { //hence 65535
      specialSize = OptionalInt.empty(); //Empty, as will need to be encoded using regular bits
    } else {
      int bytesNeededPerElement = (int) Math.ceil(log2ceil(maxiumumUpperBoundary + 1) / 8.0); //returns
      specialSize = OptionalInt.of(2 + run.size() * bytesNeededPerElement);
    }


    //Calculate the space required using just elements A, E,I, M and Q -> sizes 1,1,2,3 and 8 bytes *8 bits respectively

    int regularSize = 0;

    if (specialSize != OptionalInt.empty()) { //if empty, frequencies greater than 65535, special not possible

      for (EncodingData data : run) {
        int frequency = data.getFrequency();
        if (frequency <= 1) regularSize++;
        else if (frequency <= 255) regularSize += 2;
        else if (frequency <= 65535) regularSize += 3;
        else regularSize += 7;
      }
    }

    //Determine and do the encoding
    if (specialSize == OptionalInt.empty()) { //not feasible to do the encoding via special
      //encode all in regular.
      for (EncodingData data : run) {
        specBuilder.append(transcodeUsingRegularElements(data));
      }
    } else {
      if (specialSize.isPresent() && specialSize.getAsInt() < regularSize) { //special encoding if specialSize < regularSize
        //encode in special
        if (maxiumumUpperBoundary <= 255) {
          //encode using a single R
          specBuilder.append(transcodeUsingSpecialElements(run, 'R'));
        } else {
          //encode using a single S
          specBuilder.append(transcodeUsingSpecialElements(run, 'S'));
        }
      } else {
        //encode all in regular
        for (EncodingData data : run) {
          specBuilder.append(transcodeUsingRegularElements(data));
        }
      }
    }
    return specBuilder.toString();
  }

  /**
   * Transcodes the run of encoding data using char element
   *
   * @param run     List of encoding data
   * @param element special element to use
   * @return String the transcoded string
   */
  private static String transcodeUsingSpecialElements(List<EncodingData> run, char element) {

    StringBuilder specBuilder = new StringBuilder();
    if (element != 'R' && element != 'S') return "";

    int runCounter = 0;
    int sizeCounter = run.size();
    if (element == 'R') {
      for (EncodingData data : run) {

        if (runCounter == 0) {
          specBuilder.append('R');
          specBuilder.append(String.format("%03d", sizeCounter < 255 ? sizeCounter : 255));
        }

        specBuilder.append(String.format("%03d", data.getFrequency()));
        runCounter++;

        if (runCounter == 255) {
          runCounter = 0;
          sizeCounter -= 255;
        }
      }
    } else { //S
      for (EncodingData data : run) {
        if (runCounter == 0) {
          specBuilder.append('S');
          specBuilder.append(String.format("%03d", sizeCounter < 255 ? sizeCounter : 255));
        }

        specBuilder.append(String.format("%05d", data.getFrequency()));
        runCounter++;

        if (runCounter == 255) {
          runCounter = 0;
          sizeCounter -= 255;
        }
      }
    }

    return specBuilder.toString();
  }

  /**
   * Transcodes the encoding data point, spanning one or many bins
   *
   * @param data encoding data object
   * @return String to append
   */
  private static String transcodeUsingRegularElements(EncodingData data) {

    StringBuilder specBuilder = new StringBuilder();

    int length = data.getLength();
    int height = data.getFrequency();

    //work out bits needed for both in
    int lengthBits = 0;
    int heightBits = 0;

    if (length == 0) lengthBits = 0;
    else if (length == 1) lengthBits = 1;
    else if (length <= 255) lengthBits = 8;
    else if (length <= 65535) lengthBits = 16;
    else if (length > 65536) lengthBits = 32;

    if (height == 0) heightBits = 0;
    else if (height == 1) heightBits = 1;
    else if (height <= 255) heightBits = 8;
    else if (height <= 65535) heightBits = 16;
    else if (height > 65536) heightBits = 32;

    try {
      char token = TranscodingToken.determineToken(height, length);
      specBuilder.append(token);
    } catch (InvalidTokenException ite) {
      System.out.println(ite.getMessage());
      System.exit(0);
    }

    //get the maximum numbers for amount of bits - i.e for 16, 65335 (2^ bits - 1)

    specBuilder.append((length == 0 || length == 1) ? "" : String.format("%0" + String.valueOf((int) (Math.pow(2, lengthBits))).length() + "d", length));
    specBuilder.append((height == 0 || height == 1) ? "" : String.format("%0" + String.valueOf((int) (Math.pow(2, heightBits))).length() + "d", height));

    return specBuilder.toString();
  }
}
