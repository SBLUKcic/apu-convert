package com.sbl.compression.dependencies;

import com.sbl.common.AlignmentHistogram;
import com.sbl.compression.common.EncodingData;

import java.util.List;

public interface RunLengthEncoder {

  List<EncodingData> encode(AlignmentHistogram baseACH);
}
