package com.sbl.compression.dependencies;

import com.sbl.common.APUChunk;

public interface APUFormatEncoder {

  APUChunk convertToAPU(char[] rawspec);
}
