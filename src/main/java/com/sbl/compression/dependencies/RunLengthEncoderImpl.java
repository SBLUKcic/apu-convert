package com.sbl.compression.dependencies;


import com.sbl.common.AlignmentHistogram;
import com.sbl.compression.common.EncodingData;

import java.util.LinkedList;
import java.util.List;

public class RunLengthEncoderImpl implements RunLengthEncoder {

  @Override
  public List<EncodingData> encode(AlignmentHistogram alignmentHistogram) {

    List<EncodingData> encodedList = new LinkedList<>();
    //loop through the ACH. record the current frequency
    //if the next frequency is different, add a new element to the encoded map
    //if the next frquency is the same, incrmeent the current element

    int currentFrequency;
    int listPointer = 0;
    boolean firstLoop = true;

    for (Integer value : alignmentHistogram.getHistogram()) {
      if (firstLoop) {
        currentFrequency = value;
        encodedList.add(new EncodingData(1, currentFrequency));
        firstLoop = false;
      } else {
        currentFrequency = value;
        if (encodedList.get(listPointer).getFrequency() == currentFrequency) {
          encodedList.get(listPointer).incrementLength();
        } else {
          encodedList.add(new EncodingData(1, currentFrequency));
          listPointer++;
        }
      }

    }


    return encodedList;
  }
}
