package com.sbl.compression.dependencies;

import com.sbl.common.AlignmentHistogram;
import com.sbl.compression.common.EncodingData;

import java.util.List;

public interface APUTranscoder {

  String transcode(AlignmentHistogram alignmentHistogram, List<EncodingData> data);
}
