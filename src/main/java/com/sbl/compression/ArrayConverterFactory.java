package com.sbl.compression;


import com.sbl.compression.dependencies.*;


/**
 *  implementation of the convert factory interface.
 */
public class ArrayConverterFactory implements ConverterFactory {

  /**
   *  {@inheritDoc}
   */
  @Override
  public ArrayConverter createArrayConverter() {
    APUFormatEncoder formatEncoder = new APUFormatEncoderImpl();
    APUTranscoder transcoder = new APUTranscoderImpl();
    RunLengthEncoder runLengthEncoder = new RunLengthEncoderImpl();
    return new ArrayConverterImpl(transcoder, runLengthEncoder, formatEncoder);
  }
}
