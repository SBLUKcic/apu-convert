package com.sbl.compression.common;

/**
 * All lengths and heights in bits.
 * All 1's are to be ignored in writing
 * i.e 1 - 0 is transcoded as A.
 * 1 -1 is transcoded as E
 * 1 - 209 is transcoded as I209
 * 1 - 2 is transcoded as I002
 * 1 - 40000 is transcoded as M40000
 * 1 - 290 is transcoded as M00290
 */
public enum Token {

  A(1, 0),
  B(8, 0),
  C(16, 0),
  D(32, 0),
  E(1, 1),
  F(8, 1),
  G(16, 1),
  H(32, 1),
  I(1, 8),
  J(8, 8),
  K(16, 8),
  L(32, 8),
  M(1, 16),
  N(8, 16),
  O(16, 16),
  P(32, 16),
  Q(16, 32),
  Z(1, 32);


  private final int length;
  private final int height;


  Token(int length, int height) {
    this.length = length;
    this.height = height;
  }

  public int getLength() {
    return length;
  }

  public int getHeight() {
    return height;
  }

}
