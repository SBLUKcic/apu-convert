package com.sbl.compression.common;



import com.sbl.exceptions.InvalidTokenException;

import java.util.Arrays;

public class TranscodingToken {

  private static final int[] upperBoundaries;

  static {
    upperBoundaries = new int[4];
    upperBoundaries[0] = (1 << 0) - 1; //0
    upperBoundaries[1] = (1 << 1) - 1; //1
    upperBoundaries[2] = (1 << 8) - 1; //255
    upperBoundaries[3] = (1 << 16) - 1; //65535
  }

  /**
   * Gets the index of the upper boundary of the range
   * that the passed value falls into, i.e 0 for 0 (and
   * all negative value), 1 for 1, 2 for 2-255, 3 for 256-65535, and 4 for all values greater that 65535
   *
   * @param value value to process
   * @return index, 0 through to 4
   */
  private static int getIndexOfUpperBoundary(int value) {
    int index = Arrays.binarySearch(upperBoundaries, value);
    if (index < 0) {
      index = -(index + 1);
    }
    return index;
  }

  public static int getValueOfUpperBoundary(int value) {
    int index = getIndexOfUpperBoundary(value);
    return index < 4 ? upperBoundaries[index] : Integer.MAX_VALUE;
  }


  public static char determineToken(int height, int length) throws InvalidTokenException {
    if (height < 0) {
      throw new IllegalArgumentException("Invalid height: " + height);
    } else if (length < 0) {
      throw new IllegalArgumentException("Invalid length: " + length);
    } else if (height <= upperBoundaries[3]) {
      int heightIndex = getIndexOfUpperBoundary(height);
      int lengthIndex = getIndexOfUpperBoundary(length) - 1;
      return (char) ('A' + heightIndex * 4 + lengthIndex);
    } else if (length > upperBoundaries[2] && length <= upperBoundaries[3]) {
      return 'Q';
    } else if (length == 1) {
      return 'Z';
    } else {
      throw new InvalidTokenException("Token does not exist");
    }
  }
}
