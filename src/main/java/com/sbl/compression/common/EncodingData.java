package com.sbl.compression.common;


/**
 * Holds data for the run-length encoding
 */
public class EncodingData {


  /**
   * The number of bins in the run
   */
  private int length;

  /**
   * the frequency of the bins in the run
   */
  private int frequency;

  public EncodingData(int length, int frequency) {
    this.length = length;
    this.frequency = frequency;
  }

  public int getLength() {
    return length;
  }

  public void incrementLength() {
    this.length++;
  }

  public int getFrequency() {
    return frequency;
  }

  public void setFrequency(int frequency) {
    this.frequency = frequency;
  }
}