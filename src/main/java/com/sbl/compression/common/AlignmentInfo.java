package com.sbl.compression.common;

public class AlignmentInfo {

  private int code;
  private String statementString;
  private int extensionValue = 0;
  private String endseqQuery;
  private String flankseqQuery;

  public AlignmentInfo(int code, String statementString) {
    this.code = code;
    this.statementString = statementString;
  }

  public int getCode() {
    return code;
  }

  public String getStatementString() {
    return statementString;
  }

  public int getExtensionValue() {
    return extensionValue;
  }

  public void setExtensionValue(int extensionValue) {
    this.extensionValue = extensionValue;
  }

  public String getEndseqQuery() {
    return endseqQuery;
  }

  public void setEndseqQuery(String endseqQuery) {
    this.endseqQuery = endseqQuery;
  }

  public String getFlankseqQuery() {
    return flankseqQuery;
  }

  public void setFlankseqQuery(String flankseqQuery) {
    this.flankseqQuery = flankseqQuery;
  }
}
