package com.sbl.compression;

import com.sbl.common.APUChunk;
import com.sbl.common.AlignmentHistogram;
import com.sbl.compression.common.AlignmentInfo;
import com.sbl.compression.common.EncodingData;
import com.sbl.compression.dependencies.APUFormatEncoder;
import com.sbl.compression.dependencies.APUTranscoder;
import com.sbl.compression.dependencies.RunLengthEncoder;

import java.io.*;
import java.util.List;
import java.util.Map;

public class ArrayConverterImpl implements ArrayConverter {

  private APUTranscoder apuTranscoder;
  private RunLengthEncoder runLengthEncoder;
  private APUFormatEncoder formatEncoder;

  ArrayConverterImpl(APUTranscoder apuTranscoder, RunLengthEncoder runLengthEncoder, APUFormatEncoder formatEncoder) {
    this.apuTranscoder = apuTranscoder;
    this.runLengthEncoder = runLengthEncoder;
    this.formatEncoder = formatEncoder;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public APUChunk convertArrayToAPUChunk(AlignmentHistogram histogram) {

     //run length encode the alignment histogram
     List<EncodingData> encodingData = runLengthEncoder.encode(histogram);

     //translate to transcoded text
     String transcoded = apuTranscoder.transcode(histogram, encodingData);

     //return the apu chunk
     return formatEncoder.convertToAPU(transcoded.toCharArray());

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public File writeChunksToFile(List<APUChunk> apuChunks, File suppliedFile) throws IOException {
    if(suppliedFile == null) {
      throw new IOException("Supplied file is null");
    }
    if(!suppliedFile.exists()){
      if(!suppliedFile.createNewFile()){
        throw new IOException("Cannot create file " + suppliedFile.getName());
      }
    }

    if(apuChunks.size() == 0){
        throw new IOException("No apu chunks to write to file");
    }



    try(FileOutputStream outputStream = new FileOutputStream(suppliedFile)){
      byte[] pntrType = {(byte) 80, (byte) 78, (byte) 84, (byte) 82};

      long pntrStart = 0L;


      StringBuilder  pointerSPEC = createPointer(apuChunks, String.format("%010d%010d", 1, 1), pntrStart);
      int pntrSize = pointerSPEC.toString().getBytes().length;

      outputStream.write(createHeaderChunk(), 0, 8);

      outputStream.write(pntrType, 0, 4);
      outputStream.write(encode4BitStringDecimal(pntrSize), 0, 4);
      outputStream.write(pointerSPEC.toString().getBytes(), 0, pntrSize);

      for(APUChunk chunk : apuChunks){

        outputStream.write(chunk.getTYPE(), 0 ,4);
        outputStream.write(chunk.getSIZE(), 0, 4);
        outputStream.write(chunk.getSPEC(), 0, chunk.getSPEC().length);

      }



    }catch (IOException ioe){
      throw new IOException("Cannot create APU file");
    }

    return suppliedFile;
  }

  private StringBuilder createPointer(List<APUChunk> chunks, String resNumber, long startByte) {

    StringBuilder pointerSPEC = new StringBuilder();
    for (APUChunk chunk : chunks) {
      pointerSPEC.append(String.format("RES%sZ%03d%s%010d", resNumber, chunk.getrName().length(), chunk.getrName(), startByte));
      startByte += chunk.getSPEC().length + 8;
    }

    return pointerSPEC;
  }

  private static byte[] createHeaderChunk() {
    byte[] head = new byte[8];
    head[0] = (byte) 127; //NON-ASCII value, file cannot be interpreted as text
    head[1] = (byte) 65; //ASCII A
    head[2] = (byte) 80; //ASCII P
    head[3] = (byte) 85; //ASCII U
    head[4] = (byte) 1;  //current APU version //TODO make this come from a properties file
    head[5] = (byte) 0; //unused
    head[6] = (byte) 0; //unused
    head[7] = (byte) 4; //ASCII EOT. Halts accidently streaming to std output.
    return head;
  }

  private static byte[] encode4BitStringDecimal(int i) {
    byte[] result = new byte[4];
    result[0] = (byte) (i >> 24);
    result[1] = (byte) (i >> 16);
    result[2] = (byte) (i >> 8);
    result[3] = (byte) i;
    return result;
  }
}
