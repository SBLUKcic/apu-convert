package com.sbl.compression;

import com.sbl.common.APUChunk;
import com.sbl.common.AlignmentHistogram;
import com.sbl.compression.common.AlignmentInfo;

import java.io.File;
import java.io.IOException;
import java.util.List;

/** Interface containing methods for converting histograms into encoded APU chunks, and ultimately APU files
 *
 */
public interface ArrayConverter {

  /** Converts an alignment histogram into an APU Chunk
   * @param histogram histogram to encode
   * @return valid APU chunk
   */
  APUChunk convertArrayToAPUChunk(AlignmentHistogram histogram);

  /** Writes a list of apu chunks into an apu file, whilst creating pointers to describe the file
   * @param apuChunks List of apu chunks
   * @param suppliedFile file or file path to create the apu file
   * @return Valid APU file
   * @throws IOException cannot write or create the suppliedFile
   */
  File writeChunksToFile(List<APUChunk> apuChunks, File suppliedFile) throws IOException;

}
