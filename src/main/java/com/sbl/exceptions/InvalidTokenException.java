package com.sbl.exceptions;

/**
 * Specific exception when compressing or decompression APU file.
 * Should be thrown when a token used to encode is invalid.
 */
public class InvalidTokenException extends Exception {

  /**
   * create an InvalidTokenException with String message
   *
   * @param message message to be thrown by the exception
   */
  public InvalidTokenException(String message) {
    super(message);
  }
}
