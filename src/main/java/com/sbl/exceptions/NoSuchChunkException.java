package com.sbl.exceptions;

public class NoSuchChunkException extends Exception {

  public NoSuchChunkException(String message) {
    super(message);
  }
}
