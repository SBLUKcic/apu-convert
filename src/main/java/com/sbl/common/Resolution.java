package com.sbl.common;

public class Resolution {

  private int stepSize;
  private int binSize;

  public Resolution(int stepSize, int binSize) {
    this.stepSize = stepSize;
    this.binSize = binSize;
  }

  public int getStepSize() {
    return stepSize;
  }

  public int getBinSize() {
    return binSize;
  }
}
