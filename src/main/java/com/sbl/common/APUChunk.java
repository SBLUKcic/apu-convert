package com.sbl.common;

public class APUChunk {

  private byte[] TYPE;
  private byte[] SIZE;
  private byte[] SPEC;
  private String rName;

  public APUChunk(int TYPE, int SIZE, byte[] SPEC, String rName) {
    this.TYPE = encode4BitStringDecimal(TYPE);
    this.SIZE = encode4BitStringDecimal(SIZE);
    this.SPEC = SPEC;
    this.rName = rName;
  }

  public String getrName() {
    return rName;
  }

  public byte[] getTYPE() {
    return TYPE;
  }

  public byte[] getSPEC() {
    return SPEC;
  }

  public byte[] getSIZE() {
    return SIZE;
  }

  private static byte[] encode4BitStringDecimal(int i) {
    byte[] result = new byte[4];
    result[0] = (byte) (i >> 24);
    result[1] = (byte) (i >> 16);
    result[2] = (byte) (i >> 8);
    result[3] = (byte) i;
    return result;
  }
}
