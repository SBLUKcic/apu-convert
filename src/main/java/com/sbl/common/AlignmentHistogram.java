package com.sbl.common;

import com.sbl.compression.common.AlignmentInfo;

public class AlignmentHistogram {

  private int[] histogram;
  private int firstPosition;
  private int lastPosition;
  private String rname;
  private Resolution resolution;
  private AlignmentInfo alignmentInfo;

  public AlignmentHistogram(int[] histogram, int firstPosition, int lastPosition, String rname, Resolution resolution, AlignmentInfo info) {
    this.histogram = histogram;
    this.firstPosition = firstPosition;
    this.lastPosition = lastPosition;
    this.rname = rname;
    this.resolution = resolution;
    this.alignmentInfo = info;
  }

  public AlignmentInfo getAlignmentInfo() {
    return alignmentInfo;
  }

  public int[] getHistogram() {
    return histogram;
  }

  public int getFirstPosition() {
    return firstPosition;
  }

  public int getLastPosition() {
    return lastPosition;
  }

  public String getRname() {
    return rname;
  }

  public Resolution getResolution() {
    return resolution;
  }
}
