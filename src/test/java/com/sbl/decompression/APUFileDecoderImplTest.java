package com.sbl.decompression;

import com.sbl.common.AlignmentHistogram;
import com.sbl.decompression.common.DecodingResult;
import com.sbl.decompression.common.Pointer;
import com.sbl.decompression.dependencies.PointerExtractor;
import com.sbl.decompression.dependencies.PointerExtractorImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.List;

import static org.junit.Assert.*;

public class APUFileDecoderImplTest {

  private APUFileDecoder decoder;
  private DecoderFactory decoderFactory;
  private PointerExtractor pointerExtractor;

  private File apuFile = new File(System.getProperty("user.dir") + File.separator + "myTestAPU.apu");
  private File rnameTrailingZeroFile = new File(System.getProperty("user.dir") + File.separator + "trailingZeros.apu");
  private File fullAPU;

  @Before
  public void setUp() throws Exception {
    decoderFactory = new APUFileDecoderFactory();
    decoder = decoderFactory.createAPUFileDecoder();
    pointerExtractor = new PointerExtractorImpl();

    if(!apuFile.createNewFile() && !apuFile.exists()){
      fail();
    }
    FileOutputStream os = new FileOutputStream(apuFile);

    byte[] fileBytes = new byte[]{127,65,80,85,1,0,0,4,80,78,84,82,0,0,0,41,82,69,83,48,48,48,48,48,48,48,48,48,49,48,48,48,48,48,48,48,48,48,49,90,48,48,52,109,109,48,49,48,48,48,48,48,48,48,48,48,48,84,89,80,60,0,0,0,105,84,0,0,0,1,0,0,0,1,86,109,109,48,49,0,88,-55,36,115,111,109,101,82,97,110,100,111,109,83,116,97,116,101,109,101,110,116,83,116,105,110,103,87,105,116,104,83,111,109,101,73,110,102,111,0,0,0,0,87,0,0,0,0,0,0,0,0,0,10,89,0,0,0,26,69,66,4,73,2,66,4,73,3,66,4,73,4,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48};

    os.write(fileBytes);

    os.flush();
    os.close();

    if(!rnameTrailingZeroFile.createNewFile()){
      fail();
    }
    FileOutputStream tos = new FileOutputStream(rnameTrailingZeroFile);

    byte[] trailingBytes = new byte[]{127,65,80,85,1,0,0,4,80,78,84,82,0,0,0,41,82,69,83,48,48,48,48,48,48,48,48,48,49,48,48,48,48,48,48,48,48,48,49,90,48,48,52,109,109,49,48,48,48,48,48,48,48,48,48,48,48,84,89,80,60,0,0,0,105,84,0,0,0,1,0,0,0,1,86,109,109,49,48,0,88,-55,36,115,111,109,101,82,97,110,100,111,109,83,116,97,116,101,109,101,110,116,83,116,105,110,103,87,105,116,104,83,111,109,101,73,110,102,111,0,0,0,0,87,0,0,0,0,0,0,0,0,0,10,89,0,0,0,26,69,66,4,73,2,66,4,73,3,66,4,73,4,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48};
    tos.write(trailingBytes);

    tos.flush();
    tos.close();

    ClassLoader classLoader = getClass().getClassLoader();
    fullAPU = new File(classLoader.getResource("Master_library_all.embrc.culture.QVD.Mirin.43bp.q20.bseqv9.p10.v1.p.flankext.p.non-acct.uue_AE.endseq.AE.all.NN.apu").getFile());

    if(!fullAPU.exists() || fullAPU.length() == 0){
      fail();
    }

  }

  @Test
  public void givenValidAPUFile_whenPointerReads_checkReturned1PointerFormm01() throws Exception{

    List<Pointer> pointers = decoder.getFileInfo(apuFile);

    assertEquals(1, pointers.size());

  }

  @Test
  public void givenValidAPUFile_whenHistogramCreated_checkHistogramCorrectLength() throws Exception {

    List<Pointer> pointers = decoder.getFileInfo(apuFile);

    assertEquals(1, pointers.size());

    AlignmentHistogram histogram = decoder.retrieveAPUChunk(apuFile, pointers.get(0));

    assertEquals(16, histogram.getHistogram().length);

  }

  @Test
  public void givenValidAPUFile_whenBytesReturnedInSpecificRange_checkCorrect() throws Exception {

    List<Pointer> pointerList = pointerExtractor.extractPointers(fullAPU);

    assertNotEquals(0, pointerList.size());

    for(Pointer p : pointerList){
      if(p.getRname().equals("mm01")){
        //start pos for this chunk is.. W0000000000000000000003004381
        AlignmentHistogram hist = decoder.retrievePositions(fullAPU, p, 4205792, 200);

        assertEquals(4205792, hist.getFirstPosition());
        assertEquals(200, hist.getHistogram().length);
      }

    }

  }

  @Test
  public void givenValidAPUFile_whenSearchedForLastByteInAPU_checkValidOutput() throws Exception {

    List<Pointer> pointerList = pointerExtractor.extractPointers(fullAPU);

    assertNotEquals(0, pointerList.size());

    for(Pointer p : pointerList){
      if(p.getRname().equals("mm01")){
        //start pos for this chunk is.. W0000000000000000000003004381
        AlignmentHistogram full = decoder.retrieveAPUChunk(fullAPU, p);
        AlignmentHistogram hist = decoder.retrievePositions(fullAPU, p, full.getLastPosition(), 200);

        assertEquals(full.getLastPosition(), hist.getFirstPosition());
        assertEquals(200, hist.getHistogram().length);

        //should all be empty, since this is off the edge of the histogram.
        for(int i = 0; i < hist.getHistogram().length; i++){
          assertEquals(0, hist.getHistogram()[i]);
        }
      }

    }

  }



  @Test
  public void givenValidAPUFileWhereRnameHasTrailingZero_whenHistogramCreated_checkRnameCorrect() throws Exception{


    List<Pointer> pointers = decoder.getFileInfo(rnameTrailingZeroFile);

    assertEquals(1, pointers.size());

    AlignmentHistogram histogram = decoder.retrieveAPUChunk(rnameTrailingZeroFile, pointers.get(0));

    assertEquals("mm10", histogram.getRname());

    assertEquals(16, histogram.getHistogram().length);

  }



  @Test
  public void name() {

    byte[] arr = new byte[]{0,0,10,15};

    int val = convertFromByteArray4(arr);

    System.out.println(val);

  }

  private static int convertFromByteArray4(byte[] bytes){
    return ByteBuffer.wrap(bytes).getInt();
  }

  @After
  public void tearDown() {
    apuFile.delete();
    rnameTrailingZeroFile.delete();
  }
}