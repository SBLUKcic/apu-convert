package com.sbl.decompression.dependencies;

import com.sbl.common.AlignmentHistogram;
import com.sbl.decompression.common.DecodingResult;
import com.sbl.decompression.common.Pointer;
import com.sbl.decompression.dependencies.decoder.APUByteDecoder;
import com.sbl.decompression.dependencies.decoder.APUByteDecoderImpl;
import org.junit.Before;
import org.junit.Test;


import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

public class APUChunkReaderImplTest {

    private File apuFile;
    private APUChunkReader apuChunkReader;
    private PointerExtractor pointerExtractor;
    private APUByteDecoder decoder;
    private AlignmentHistogramBuilder alignmentHistogramBuilder;

    @Before
    public void setUp() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        apuFile = new File(classLoader.getResource("Master_library_all.embrc.culture.QVD.Mirin.43bp.q20.bseqv9.p10.v1.p.flankext.p.non-acct.uue_AE.endseq.AE.all.NN.apu").getFile());

        if(!apuFile.exists() || apuFile.length() == 0){
            fail();
        }

        apuChunkReader = new APUChunkReaderImpl();
        pointerExtractor = new PointerExtractorImpl();
        decoder = new APUByteDecoderImpl();
        alignmentHistogramBuilder = new AlignmentHistogramBuilderImpl();
    }

    @Test
    public void giveValidAPUFile_whenBytesReturned_checkBytesCorrect() throws Exception{

        List<Pointer> pointerList = pointerExtractor.extractPointers(apuFile);

        assertNotEquals(0, pointerList.size());

        for(Pointer p : pointerList){
            byte[] b = apuChunkReader.readChunk(apuFile, p.getBytePosition());
            assertNotNull(b);
            assertNotEquals(0, b.length);
        }

    }

    @Test
    public void givenValidAPUFile_whenBytesReturnedInSpecificRange_checkCorrect() throws Exception {

        List<Pointer> pointerList = pointerExtractor.extractPointers(apuFile);

        assertNotEquals(0, pointerList.size());

        for(Pointer p : pointerList){
            if(p.getRname().equals("mm01")){
                //start pos for this chunk is.. W0000000000000000000003004381
                byte[] b = apuChunkReader.readChunk(apuFile, p.getBytePosition(), 4205792);
                assertNotNull(b);
                assertNotEquals(0, b.length);
                DecodingResult res = decoder.decodeBytes(b);
                AlignmentHistogram alignmentHistogram =  alignmentHistogramBuilder.buildAlignmentHistogram(res);
                assertEquals(4199299, alignmentHistogram.getFirstPosition());
                assertEquals(22226, alignmentHistogram.getHistogram().length);
            }

        }

    }
}