package com.sbl.decompression.dependencies.decoder;

import com.sbl.common.APUChunk;
import com.sbl.compression.dependencies.APUFormatEncoder;
import com.sbl.compression.dependencies.APUFormatEncoderImpl;
import com.sbl.decompression.common.DecodingResult;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class APUByteDecoderImplTest {

  private APUByteDecoder decoder;
  private APUFormatEncoder encoder;

  @Before
  public void setUp() throws Exception {
    decoder = new APUByteDecoderImpl();
    encoder = new APUFormatEncoderImpl();
  }

  @Test
  public void givenValidBytes_whenDecoded_checkDecodingCorrect() {


    byte[] bytes = new byte[]{84,0,0,0,1,0,0,0,1,86,109,109,48,49,0,88,-55,16,115,111,109,101,82,97,110,100,111,109,83,116,114,105,110,103,0,0,0,0,87,0,0,0,0,0,0,0,0,0,10,89,0,0,0,26,69,66,4,73,2,66,4,73,3,66,4,73,4,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48};

    DecodingResult result = decoder.decodeBytes(bytes);

    String expectedResult = "T00000000010000000001Vmm01000X201016someRandomString00000000000W0000000000000000000000000010Y0000000026EB004I002B004I003B004I004000000000000000000";

    assertEquals(expectedResult, new String(result.getCode()));
    System.out.println(new String(result.getCode()));
  }

  @Test
  public void givenValidAPUWithRString_whenDecoded_checkDecodingCorrect() {

    String myExpectedResults = "T00000000010000000001Vmm01000X201016statementAboutIt00000000000W0000000000000000000000000010Y0000000026R008007022000013048003004001000000000000000000";

    APUChunk chunk = encoder.convertToAPU(myExpectedResults.toCharArray());

    DecodingResult result = decoder.decodeBytes(chunk.getSPEC());

    assertEquals(myExpectedResults, new String(result.getCode()));

  }

  @Test
  public void givenValidAPUWithSString_whenDecoded_checkDecodingCorrect() {

    String myExpectedResults = "T00000000010000000001Vmm01000X201016statementAboutIt00000000000W0000000000000000000000000010Y0000000026S009000072200000000655341300048000300000000400001000000000000000000";

    APUChunk chunk = encoder.convertToAPU(myExpectedResults.toCharArray());

    DecodingResult result = decoder.decodeBytes(chunk.getSPEC());

    assertEquals(myExpectedResults, new String(result.getCode()));

  }
}