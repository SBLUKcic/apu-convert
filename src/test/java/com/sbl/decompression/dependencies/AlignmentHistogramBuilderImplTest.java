package com.sbl.decompression.dependencies;

import com.sbl.common.AlignmentHistogram;
import com.sbl.decompression.common.DecodingResult;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AlignmentHistogramBuilderImplTest {

  private AlignmentHistogramBuilder builder;

  @Before
  public void setUp() throws Exception {
    builder = new AlignmentHistogramBuilderImpl();
  }

  @Test
  public void givenValidSpecString_whenACHBuilt_checkACHCorrect() throws Exception{

    String codeString = "T00000000010000000001Vmm01000X201016someRandomString00000000000W0000000000000000000000000010Y0000000026EB004I002B004I003B004I004000000000000000000";
    DecodingResult result = new DecodingResult(codeString.toCharArray(), 16);

    AlignmentHistogram histogram = builder.buildAlignmentHistogram(result);

    int[] expectedResult = new int[]{1,0,0,0,0,2,0,0,0,0,3,0,0,0,0,4};

    assertEquals(expectedResult.length, histogram.getHistogram().length);


    for(int i = 0; i < expectedResult.length; i++){
      assertEquals(expectedResult[i], histogram.getHistogram()[i]);
    }

    System.out.println(histogram.getFirstPosition());
    System.out.println(histogram.getLastPosition());

  }
}