package com.sbl.compression.dependencies;

import com.sbl.common.AlignmentHistogram;
import com.sbl.common.Resolution;
import com.sbl.compression.common.AlignmentInfo;
import com.sbl.compression.common.EncodingData;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RunLengthEncoderImplTest {

  private RunLengthEncoder encoder;

  @Before
  public void setUp() throws Exception {
    encoder = new RunLengthEncoderImpl();
  }

  @Test
  public void givenSmallHistogram_whenRunLengthEncoded_checkEncodingCorrect() {


    int[] arr = new int[]{1,0,0,0,0,2,0,0,0,0,3,0,0,0,0,4};

    AlignmentHistogram histogram = new AlignmentHistogram(arr, 10, 26, "mm01", new Resolution(1,1), new AlignmentInfo(201, "hi"));

    EncodingData first = new EncodingData(1,1);
    EncodingData firstZeros = new EncodingData(4,0);
    EncodingData two = new EncodingData(1,2);
    EncodingData secondZeros = new EncodingData(4, 0);
    EncodingData three = new EncodingData(1, 3);
    EncodingData lastZeros = new EncodingData(4,0 );
    EncodingData last = new EncodingData(1,4);

    List<EncodingData> expectedData = Arrays.asList(first, firstZeros, two, secondZeros, three, lastZeros, last);

    List<EncodingData> data = encoder.encode(histogram);

    //givenValidHistogramAndInfo_whenTranscoded_checkTranscodingCorrect check size is equal
    assertEquals(expectedData.size(), data.size());


    for(int i = 0; i < data.size(); i++){
      assertEquals(expectedData.get(i).getFrequency(), data.get(i).getFrequency());
      assertEquals(expectedData.get(i).getLength(), data.get(i).getLength());
    }


  }
}