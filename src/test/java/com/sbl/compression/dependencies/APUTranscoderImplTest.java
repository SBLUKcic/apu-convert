package com.sbl.compression.dependencies;

import com.sbl.common.AlignmentHistogram;
import com.sbl.common.Resolution;
import com.sbl.compression.common.AlignmentInfo;
import com.sbl.compression.common.EncodingData;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class APUTranscoderImplTest {

  private APUTranscoder transcoder;

  private RunLengthEncoder encoder;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    transcoder = new APUTranscoderImpl();
    encoder = new RunLengthEncoderImpl();
  }

  @Test
  public void givenValidHistogramAndInfo_whenTranscoded_checkTranscodingCorrect() {

    int[] arr = new int[]{1,0,0,0,0,2,0,0,0,0,3,0,0,0,0,4};

    AlignmentInfo info = new AlignmentInfo(201, "someRandomString");

    AlignmentHistogram histogram = new AlignmentHistogram(arr, 10, 26, "mm01", new Resolution(1,1), info);

    EncodingData first = new EncodingData(1,1);
    EncodingData firstZeros = new EncodingData(4,0);
    EncodingData two = new EncodingData(1,2);
    EncodingData secondZeros = new EncodingData(4, 0);
    EncodingData three = new EncodingData(1, 3);
    EncodingData lastZeros = new EncodingData(4,0 );
    EncodingData last = new EncodingData(1,4);

    List<EncodingData> expectedData = Arrays.asList(first, firstZeros, two, secondZeros, three, lastZeros, last);



    String result = transcoder.transcode(histogram, expectedData);

    assertEquals("T00000000010000000001Vmm01000X201016someRandomString00000000000W0000000000000000000000000010Y0000000026EB004I002B004I003B004I004000000000000000000", result);

  }

  @Test
  public void givenLongHistogram_whenTranscodeD_checkTranscodingCorrect() {

    int[] arr = new int[]{1,0,0,0,0,2,0,0,0,0,3,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,66,6,6,6,6,6,6,256,3450,0,0,0,0,0,0,0,0,0,0,0};

    AlignmentInfo info = new AlignmentInfo(201, "someRandomString");

    AlignmentHistogram histogram = new AlignmentHistogram(arr, 10, 10 + arr.length, "mm01", new Resolution(1,1), info);

    List<EncodingData> data = encoder.encode(histogram);

    assertEquals("T00000000010000000001Vmm01000X201016someRandomString00000000000W0000000000000000000000000010Y0000000135EB004I002B004I003B004I004B026J016005J047006I066J006006M00256M03450B011000000000000000000", transcoder.transcode(histogram, data));

  }

  @Test
  public void givenLongHistogram_whenTranscodeD_checkDataContainsWelement() {
    Random random = new Random();
    int[] arr = new int[100000];

    for(int i = 0; i < 100000; i++){
      if(i < 90000) {
        arr[i] = random.nextInt(65535);
      }else{
        arr[i] = 1;
      }
    }



    AlignmentInfo info = new AlignmentInfo(201, "someOtherString");

    AlignmentHistogram histogram = new AlignmentHistogram(arr, 10, 10 + arr.length, "mm01", new Resolution(1,1), info);

    List<EncodingData> data = encoder.encode(histogram);

    String result = transcoder.transcode(histogram, data);

    int wCount = 0;
    for(int i = 0; i < result.length(); i++){
      if(result.charAt(i) == 'W'){
        wCount++;
      }
    }

    assertTrue(wCount >= 2);

  }


  @Test
  public void givenValidHistogramOfAllDifferentNumbersUnder255_whenTranscoded_checkSpecialRElementsUsed() {

    int[] arr = new int[]{7,22,0,13,48,3,4,1};

    AlignmentInfo info = new AlignmentInfo(201, "statementAboutIt");

    AlignmentHistogram histogram = new AlignmentHistogram(arr, 10, 26, "mm01", new Resolution(1,1), info);


    List<EncodingData> expectedData = encoder.encode(histogram);


    String result = transcoder.transcode(histogram, expectedData);

    assertEquals("T00000000010000000001Vmm01000X201016statementAboutIt00000000000W0000000000000000000000000010Y0000000026R008007022000013048003004001000000000000000000", result);
  }

  @Test
  public void givenValidHistogramOfAllDifferentNumbersOneOver255_whenTranscoded_checkSpecialSElementsUsed() {

    int[] arr = new int[]{7,22000,0,65534,13000,48000,30000,4,1};

    AlignmentInfo info = new AlignmentInfo(201, "statementAboutIt");

    AlignmentHistogram histogram = new AlignmentHistogram(arr, 10, 26, "mm01", new Resolution(1,1), info);


    List<EncodingData> expectedData = encoder.encode(histogram);


    String result = transcoder.transcode(histogram, expectedData);

    assertEquals("T00000000010000000001Vmm01000X201016statementAboutIt00000000000W0000000000000000000000000010Y0000000026S009000072200000000655341300048000300000000400001000000000000000000", result);
  }


}