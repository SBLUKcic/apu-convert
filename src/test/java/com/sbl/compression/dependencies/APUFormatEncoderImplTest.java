package com.sbl.compression.dependencies;

import com.sbl.common.APUChunk;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class APUFormatEncoderImplTest {

  private APUFormatEncoder formatEncoder;

  @Before
  public void setUp() throws Exception {
    formatEncoder = new APUFormatEncoderImpl();
  }

  @Test
  public void givenValidByteSpec_whenEncoded_checkEncodingCorrect() {

    String spec = "T00000000010000000001Vmm01000X201016someRandomString00000000000W0000000000000000000000000010Y0000000026EB004I002B004I003B004I004000000000000000000";

    APUChunk chunk = formatEncoder.convertToAPU(spec.toCharArray());

    assertEquals(85, chunk.getSIZE()[3]);

    byte[] expected = new byte[]{84,0,0,0,1,0,0,0,1,86,109,109,48,49,0,88,-55,16,115,111,109,101,82,97,110,100,111,109,83,116,114,105,110,103,0,0,0,0,87,0,0,0,0,0,0,0,0,0,10,89,0,0,0,26,69,66,4,73,2,66,4,73,3,66,4,73,4,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48};

    assertEquals("mm01", chunk.getrName());

    assertEquals(expected.length, chunk.getSPEC().length);

    for(int i = 0; i < chunk.getSPEC().length; i++){
      assertEquals(expected[i], chunk.getSPEC()[i]);
    }

  }
}