package com.sbl.compression;

import com.sbl.common.APUChunk;
import com.sbl.common.AlignmentHistogram;
import com.sbl.common.Resolution;
import com.sbl.compression.common.AlignmentInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class ArrayConverterImplTest {

  private ArrayConverter arrayConverter;
  private ConverterFactory converterFactory;
  private File newAPUFile;

  @Before
  public void setUp() throws Exception {
    converterFactory = new ArrayConverterFactory();
    arrayConverter = converterFactory.createArrayConverter();

    newAPUFile = new File(System.getProperty("user.dir") + File.separator + "myNewAPUFile.apu");
  }

  @Test
  public void givenValidHistogram_whenConvertedtoAPU_checkAPUChunkCorrect() throws IOException {

    int[] arr = new int[]{1,0,0,0,0,2,0,0,0,0,3,0,0,0,0,4};
    AlignmentInfo info = new AlignmentInfo(201, "someRandomStatementStingWithSomeInfo");
    AlignmentHistogram histogram = new AlignmentHistogram(arr, 10, 26, "mm01", new Resolution(1,1), info);


    APUChunk chunk = arrayConverter.convertArrayToAPUChunk(histogram);

    List<APUChunk> chunks = new ArrayList<>();
    chunks.add(chunk);

    File output = arrayConverter.writeChunksToFile(chunks, newAPUFile);

    assertEquals(170, output.length());


  }

  @Test
  @Ignore
  public void givenValidMultipleHistograms_whenConvertedtoAPU_checkAPUChunkCorrect() throws IOException {

    int[] arr = new int[]{1,0,0,0,0,2,0,0,0,0,3,0,0,0,0,4};
    AlignmentInfo info = new AlignmentInfo(201, "someRandomStatementStingWithSomeInfo");
    AlignmentInfo info2 = new AlignmentInfo(201, "someRandomStatementStingWithSomeMoreAwsomelyLongInfo");
    AlignmentHistogram histogram = new AlignmentHistogram(arr, 10, 26, "mm01", new Resolution(1,1), info);
    AlignmentHistogram histogram2 = new AlignmentHistogram(arr, 10, 26, "mm02", new Resolution(1,1), info2);

    APUChunk chunk = arrayConverter.convertArrayToAPUChunk(histogram);
    APUChunk chunk2 = arrayConverter.convertArrayToAPUChunk(histogram2);

    List<APUChunk> chunks = new ArrayList<>();
    chunks.add(chunk);
    chunks.add(chunk2);


    File output = arrayConverter.writeChunksToFile(chunks, newAPUFile);

    assertEquals(340, output.length());


  }

  @Test
  public void givenValidHistogramWithRnameTrailingZero_whenConverted_checkRNamePersistedCorrectly() throws Exception {

    int[] arr = new int[]{1,0,0,0,0,2,0,0,0,0,3,0,0,0,0,4};
    AlignmentInfo info = new AlignmentInfo(201, "someRandomStatementStingWithSomeInfo");
    AlignmentHistogram histogram = new AlignmentHistogram(arr, 10, 26, "mm10", new Resolution(1,1), info);


    APUChunk chunk = arrayConverter.convertArrayToAPUChunk(histogram);

    List<APUChunk> chunks = new ArrayList<>();
    chunks.add(chunk);




    File output = arrayConverter.writeChunksToFile(chunks, newAPUFile);


    InputStream inputStream = new FileInputStream(newAPUFile);

    byte[] bytes = new byte[170];

    inputStream.read(bytes);

    for(byte b : bytes){
      System.out.print(b + ",");
    }

    assertEquals(170, output.length());


  }

  @After
  public void tearDown() throws Exception {
    if(newAPUFile.exists()){
      if(!newAPUFile.delete()){
        System.out.println("apu file from array converter test not deleted");
      }
    }
  }
}