[![](https://jitpack.io/v/com.gitlab.SBLUKcic/apu-convert.svg)](https://jitpack.io/#com.gitlab.SBLUKcic/apu-convert)
[![coverage report](https://gitlab.com/SBLUKcic/apu-convert/badges/master/coverage.svg)](https://gitlab.com/SBLUKcic/apu-convert/commits/master)

APU Convert is designed to convert alignment coverage histograms to and from the .apu format.

To install:

Add it in your root build.gradle at the end of repositories:

```
allprojects {
  repositories {
	...
    maven { url 'https://jitpack.io' }
  }
}
```

Add the dependency:

```
dependencies {
  implementation 'com.gitlab.SBLUKcic:apu-convert:Tag'
}
```